use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :dlt, DLT.Repo,
  username: "postgres",
  password: "postgres",
  database: "dlt_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :dlt, DLTWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :dlt, :providers,
  hetzner: %{api_token: "test hetzner token"},
  vultr: %{api_token: "test vultr token"}

config :tesla, DLT.Cloud.Provider.Hetzner, adapter: DLT.Cloud.Provider.HetznerMock
config :tesla, DLT.Cloud.Provider.Vultr, adapter: DLT.Cloud.Provider.VultrMock

# empty map instead of an empty list is a hack to override config
# rather than merge default one with empty list
config :libcluster, topologies: %{}

config :dlt, Oban, queues: false, plugins: false
