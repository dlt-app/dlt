# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :dlt,
  namespace: DLT,
  ecto_repos: [DLT.Repo]

# Configures the endpoint
config :dlt, DLTWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "DXtLD62r0SBeQuSahYdfLFtYXeZu8UNiTmHLZ/nU0Kaxjmw72rj7Hix4u91cZFPW",
  render_errors: [view: DLTWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: DLT.PubSub,
  live_view: [signing_salt: "0J7V2g11"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :libcluster,
  topologies: [
    hetzner: [
      strategy: DLT.Cluster.Strategy.Hetzner,
      config: [
        polling_interval: 5_000
      ]
    ],
    vultr: [
      strategy: DLT.Cluster.Strategy.Vultr,
      config: [
        polling_interval: 5_000
      ]
    ]
  ]

config :dlt, Oban,
  repo: DLT.Repo,
  plugins: [],
  queues: [server_cleanup: 20]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

# Import secret config specific for current environment.
# These configs are not tracked by VCS, so there may be no such file.
if File.exists?("./config/#{Mix.env()}.secret.exs") do
  import_config "#{Mix.env()}.secret.exs"
end
