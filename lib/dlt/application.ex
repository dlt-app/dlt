defmodule DLT.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      DLT.Repo,
      # Start the Telemetry supervisor
      DLTWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: DLT.PubSub},
      # Start the Endpoint (http/https)
      DLTWeb.Endpoint,
      {Oban, Application.fetch_env!(:dlt, Oban)},
      {Cluster.Supervisor,
       [Application.fetch_env!(:libcluster, :topologies), [name: DLT.ClusterSupervisor]]},
      {DynamicSupervisor,
       strategy: :one_for_one, name: DLT.Tests.Scenario.Session.ManagerSupervisor}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: DLT.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    DLTWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
