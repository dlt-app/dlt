defmodule DLT.Ecto.Migration.Enum do
  def create_type(name, enum) do
    create_query = """
    DO
    $do$
    BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = '#{name}') THEN
      CREATE TYPE #{name} AS ENUM (#{enum |> Enum.map(&"'#{&1}'") |> Enum.join(",")});
    END IF;
    END;
    $do$
    """

    drop_query = """
    DO
    $do$
    BEGIN
    IF EXISTS (SELECT 1 FROM pg_type WHERE typname = '#{name}') THEN
      DROP TYPE #{name};
    END IF;
    END;
    $do$
    """

    Ecto.Migration.execute(create_query, drop_query)
  end
end
