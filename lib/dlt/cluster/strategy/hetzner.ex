defmodule DLT.Cluster.Strategy.Hetzner do
  use GenServer
  use Cluster.Strategy

  alias Cluster.Strategy.State

  @app_name "dlt-worker-hetzner"

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  @impl GenServer
  def init([%State{} = default_state]) do
    state =
      default_state
      |> Map.put(:meta, MapSet.new())
      |> Map.put(:connect, {__MODULE__, :connect_node, []})
      |> load()

    {:ok, state}
  end

  @impl GenServer
  def handle_info(:timeout, state) do
    send(self(), :load)
    {:noreply, state}
  end

  @impl GenServer
  def handle_info(:load, state) do
    {:noreply, load(state)}
  end

  defp load(
         %State{
           topology: topology,
           connect: connect_fun,
           disconnect: disconnect_fun,
           list_nodes: list_nodes
         } = state
       ) do
    case get_nodes(state) do
      {:ok, new_nodes} ->
        added = MapSet.difference(new_nodes, state.meta)
        removed = MapSet.difference(state.meta, new_nodes)

        new_nodes =
          case Cluster.Strategy.disconnect_nodes(
                 topology,
                 disconnect_fun,
                 list_nodes,
                 MapSet.to_list(removed)
               ) do
            :ok ->
              new_nodes

            {:error, bad_nodes} ->
              Enum.reduce(bad_nodes, new_nodes, fn {node, _}, acc ->
                MapSet.put(acc, node)
              end)
          end

        new_nodes =
          case Cluster.Strategy.connect_nodes(
                 topology,
                 connect_fun,
                 list_nodes,
                 MapSet.to_list(added)
               ) do
            :ok ->
              new_nodes

            {:error, bad_nodes} ->
              Enum.reduce(bad_nodes, new_nodes, fn {node, _}, acc ->
                MapSet.delete(acc, node)
              end)
          end

        Process.send_after(self(), :load, state.config[:polling_interval])
        %{state | :meta => new_nodes}

      _ ->
        Process.send_after(self(), :load, state.config[:polling_interval])
        state
    end
  end

  defp get_nodes(%State{topology: topology}) do
    case DLT.Cloud.Provider.Hetzner.list_servers() do
      {:ok, servers} ->
        node_names = Enum.map(servers, &:"#{@app_name}@#{&1["public_net"]["ipv4"]["ip"]}")

        {:ok, MapSet.new(node_names)}

      {:error, error} ->
        Cluster.Logger.error(topology, "Could not list Hetzner servers: #{inspect(error)}")
        {:error, []}
    end
  end

  def connect_node(nodename) do
    with true <- :net_kernel.connect_node(nodename) do
      worker = DLT.Tests.get_worker_by_nodename(nodename)
      {:ok, new_worker} = DLT.Tests.update_worker(worker, %{status: :online})

      # will try to delete in 50 minutes, next remove attempts will be occuring once every 60 minutes
      job_params = [scheduled_at: DateTime.add(DateTime.utc_now(), 3000, :second)]

      %{"worker_id" => new_worker.id}
      |> DLT.Oban.ServerCleanup.new(job_params)
      |> Oban.insert()

      # if the worker has non-empty test relation, notify the test channel about the connection
      unless is_nil(new_worker.session_id) do
        Phoenix.PubSub.broadcast(
          DLT.PubSub,
          "session:#{new_worker.session_id}",
          {:worker_connected, new_worker}
        )
      end

      true
    end
  end
end
