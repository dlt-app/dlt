defmodule DLT.Cluster.Strategy.Vultr do
  use GenServer
  use Cluster.Strategy

  alias Cluster.Strategy.State

  @nodename_prefix "dlt-worker-vultr"

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  @impl GenServer
  def init([%State{} = default_state]) do
    state =
      default_state
      |> Map.put(:meta, MapSet.new())
      |> Map.put(:connect, {__MODULE__, :connect_node, []})
      |> Map.put(:disconnect, {__MODULE__, :disconnect_node, []})
      |> load()

    {:ok, state}
  end

  @impl GenServer
  def handle_info(:timeout, state) do
    send(self(), :load)
    {:noreply, state}
  end

  @impl GenServer
  def handle_info(:load, state) do
    {:noreply, load(state)}
  end

  defp load(
         %State{
           topology: topology,
           connect: connect_fun,
           disconnect: disconnect_fun,
           list_nodes: list_nodes
         } = state
       ) do
    case get_nodes(state) do
      {:ok, new_nodes} ->
        added = MapSet.difference(new_nodes, state.meta)
        removed = MapSet.difference(state.meta, new_nodes)

        new_nodes =
          case Cluster.Strategy.disconnect_nodes(
                 topology,
                 disconnect_fun,
                 list_nodes,
                 MapSet.to_list(removed)
               ) do
            :ok ->
              new_nodes

            {:error, bad_nodes} ->
              Enum.reduce(bad_nodes, new_nodes, fn {node, _}, acc ->
                MapSet.put(acc, node)
              end)
          end

        new_nodes =
          case Cluster.Strategy.connect_nodes(
                 topology,
                 connect_fun,
                 list_nodes,
                 MapSet.to_list(added)
               ) do
            :ok ->
              new_nodes

            {:error, bad_nodes} ->
              Enum.reduce(bad_nodes, new_nodes, fn {node, _}, acc ->
                MapSet.delete(acc, node)
              end)
          end

        Process.send_after(self(), :load, state.config[:polling_interval])
        %{state | :meta => new_nodes}

      _ ->
        Process.send_after(self(), :load, state.config[:polling_interval])
        state
    end
  end

  defp get_nodes(%State{topology: topology}) do
    case DLT.Cloud.Provider.Vultr.list_servers() do
      {:ok, servers} ->
        nodes =
          servers
          |> Enum.reject(&(&1["main_ip"] == "0.0.0.0"))
          |> Enum.map(&%{id: &1["id"], ipv4: &1["main_ip"]})

        {:ok, MapSet.new(nodes)}

      {:error, error} ->
        Cluster.Logger.error(topology, "Could not list Vultr servers: #{inspect(error)}")
        {:error, []}
    end
  end

  def connect_node(%{id: id, ipv4: ipv4}) do
    nodename = :"#{@nodename_prefix}@#{ipv4}"

    with true <- :net_kernel.connect_node(nodename) do
      worker = DLT.Tests.get_worker_by_remote_id(:vultr, id)
      {:ok, new_worker} = DLT.Tests.update_worker(worker, %{status: :online, ipv4: ipv4})

      # will try to delete in 50 minutes, next remove attempts will be occuring once every 60 minutes
      job_params = [scheduled_at: DateTime.add(DateTime.utc_now(), 3000, :second)]

      %{"worker_id" => new_worker.id}
      |> DLT.Oban.ServerCleanup.new(job_params)
      |> Oban.insert()

      # if the worker has non-empty test relation, notify the test channel about the connection
      unless is_nil(new_worker.session_id) do
        Phoenix.PubSub.broadcast(
          DLT.PubSub,
          "session:#{new_worker.session_id}",
          {:worker_connected, new_worker}
        )
      end

      true
    end
  end

  def disconnect_node(%{id: _id, ipv4: ipv4}) do
    nodename = :"#{@nodename_prefix}@#{ipv4}"
    :erlang.disconnect_node(nodename)
  end
end
