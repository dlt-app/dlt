defmodule DLT.Oban.ServerCleanup do
  use Oban.Worker, queue: :server_cleanup

  @impl Oban.Worker
  def perform(%Oban.Job{args: %{"worker_id" => worker_id}}) do
    worker = DLT.Tests.get_worker!(worker_id)

    if worker.status == :busy do
      IO.puts("Server is busy, will try to remove again in 1 hour")
      job_params = [scheduled_at: DateTime.add(DateTime.utc_now(), 3_600, :second)]

      %{"worker_id" => worker.id}
      |> new(job_params)
      |> Oban.insert()

      :ok
    else
      IO.puts("#{DateTime.utc_now()} - Removing server: #{inspect(worker)}")
      provider = Module.concat(DLT.Cloud.Provider, String.capitalize("#{worker.provider}"))
      provider.delete_server(worker)
    end
  end
end
