defmodule DLT.Tests do
  @moduledoc """
  The Tests context.
  """

  import Ecto.Query, warn: false
  alias DLT.Repo

  alias DLT.Tests.Scenario

  @doc """
  Returns the list of scenarios.

  ## Examples

      iex> list_scenarios()
      [%Scenario{}, ...]

  """
  @spec list_scenarios :: [Scenario.t()]
  def list_scenarios do
    Scenario
    |> Repo.all()
    |> Repo.preload(:sessions)
  end

  @doc """
  Gets a single scenario.

  Raises `Ecto.NoResultsError` if the Scenario does not exist.

  ## Examples

      iex> get_scenario!(123)
      %Scenario{}

      iex> get_scenario!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_scenario!(integer()) :: Scenario.t() | no_return()
  def get_scenario!(id) do
    Scenario
    |> Repo.get!(id)
    |> Repo.preload([:requests, :scenario_locations, [sessions: :results]])
  end

  @doc """
  Creates a scenario.

  ## Examples

      iex> create_scenario(%{field: value})
      {:ok, %Scenario{}}

      iex> create_scenario(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_scenario(map()) :: {:ok, Scenario.t()} | {:error, Ecto.Changeset.t()}
  def create_scenario(attrs) do
    %Scenario{}
    |> Scenario.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a scenario.

  ## Examples

      iex> update_scenario(scenario, %{field: new_value})
      {:ok, %Scenario{}}

      iex> update_scenario(scenario, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_scenario(Scenario.t(), map()) :: {:ok, Scenario.t()} | {:error, Ecto.Changeset.t()}
  def update_scenario(%Scenario{} = scenario, attrs) do
    scenario
    |> Scenario.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a scenario.

  ## Examples

      iex> delete_scenario(scenario)
      {:ok, %Scenario{}}

      iex> delete_scenario(scenario)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_scenario(Scenario.t()) :: {:ok, Scenario.t()} | {:error, Ecto.Changeset.t()}
  def delete_scenario(%Scenario{} = scenario) do
    Repo.delete(scenario)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking scenario changes.

  ## Examples

      iex> change_scenario(scenario)
      %Ecto.Changeset{data: %Scenario{}}

  """
  @spec change_scenario(Scenario.t(), map()) :: Ecto.Changeset.t()
  def change_scenario(%Scenario{} = scenario, attrs \\ %{}) do
    Scenario.changeset(scenario, attrs)
  end

  alias DLT.Tests.Scenario.Request

  @doc """
  Returns the list of scenario_requests.

  ## Examples

      iex> list_scenario_requests()
      [%Request{}, ...]

  """
  @spec list_scenario_requests :: [Scenario.Request.t()]
  def list_scenario_requests do
    Repo.all(Request)
  end

  @doc """
  Gets a single request.

  Raises `Ecto.NoResultsError` if the Request does not exist.

  ## Examples

      iex> get_request!(123)
      %Request{}

      iex> get_request!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_request!(integer) :: Scenario.Request.t() | no_return()
  def get_request!(id), do: Repo.get!(Request, id)

  @doc """
  Creates a request.

  ## Examples

      iex> create_request(%{field: value})
      {:ok, %Request{}}

      iex> create_request(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_request(map()) :: {:ok, Scenario.Request.t()} | {:error, Ecto.Changeset.t()}
  def create_request(attrs) do
    %Request{}
    |> Request.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a request.

  ## Examples

      iex> update_request(request, %{field: new_value})
      {:ok, %Request{}}

      iex> update_request(request, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_request(Scenario.Request.t(), map()) ::
          {:ok, Scenario.Request.t()} | {:error, Ecto.Changeset.t()}
  def update_request(%Request{} = request, attrs) do
    request
    |> Request.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a request.

  ## Examples

      iex> delete_request(request)
      {:ok, %Request{}}

      iex> delete_request(request)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_request(Scenario.Request.t()) ::
          {:ok, Scenario.Request.t()} | {:error, Ecto.Changeset.t()}
  def delete_request(%Request{} = request) do
    Repo.delete(request)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking request changes.

  ## Examples

      iex> change_request(request)
      %Ecto.Changeset{data: %Request{}}

  """
  @spec change_request(Scenario.Request.t()) :: Ecto.Changeset.t()
  def change_request(%Request{} = request, attrs \\ %{}) do
    Request.changeset(request, attrs)
  end

  alias DLT.Tests.Scenario.Session

  @doc """
  Returns the list of scenario_sessions.

  ## Examples

      iex> list_scenario_sessions()
      [%Session{}, ...]

  """
  @spec list_scenario_sessions :: [Scenario.Session.t()]
  def list_scenario_sessions do
    Repo.all(Session)
  end

  @doc """
  Gets a single session.

  Raises `Ecto.NoResultsError` if the Session does not exist.

  ## Examples

      iex> get_session!(123)
      %Session{}

      iex> get_session!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_session!(integer()) :: Scenario.Session.t() | no_return()
  def get_session!(id), do: Repo.get!(Session, id)

  @doc """
  Creates a session.

  ## Examples

      iex> create_session(%{field: value})
      {:ok, %Session{}}

      iex> create_session(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_session_from_scenario(Scenario.t()) ::
          {:ok, Scenario.Session.t()} | {:error, Ecto.Changeset.t()}
  def create_session_from_scenario(scenario) do
    %Session{}
    |> Session.changeset(%{
      "scenario_id" => scenario.id,
      "rps" => scenario.rps,
      "duration" => scenario.duration,
      "nodes" => scenario.nodes,
      "requests" => scenario.requests,
      "status" => :starting
    })
    |> Repo.insert()
  end

  @doc """
  Updates a session.

  ## Examples

      iex> update_session(session, %{field: new_value})
      {:ok, %Session{}}

      iex> update_session(session, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_session(Scenario.Session.t(), map()) ::
          {:ok, Scenario.Session.t()} | {:error, Ecto.Changeset.t()}
  def update_session(%Session{} = session, attrs) do
    session
    |> Session.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a session.

  ## Examples

      iex> delete_session(session)
      {:ok, %Session{}}

      iex> delete_session(session)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_session(Scenario.Session.t()) ::
          {:ok, Scenario.Session.t()} | {:error, Ecto.Changeset.t()}
  def delete_session(%Session{} = session) do
    Repo.delete(session)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking session changes.

  ## Examples

      iex> change_session(session)
      %Ecto.Changeset{data: %Session{}}

  """
  @spec change_session(Scenario.Session.t()) :: Ecto.Changeset.t()
  def change_session(%Session{} = session, attrs \\ %{}) do
    Session.changeset(session, attrs)
  end

  alias DLT.Tests.Worker

  @doc """
  Returns the list of workers.

  ## Examples

      iex> list_workers()
      [%Worker{}, ...]

  """
  @spec list_workers :: [Worker.t()]
  def list_workers do
    Repo.all(Worker)
  end

  def list_vultr_workers_with_unassigned_ip do
    query =
      from w in Worker,
        where: w.provider == :vultr,
        where: w.ipv4 == "0.0.0.0"

    Repo.all(query)
  end

  @doc """
  Gets a single worker.

  Raises `Ecto.NoResultsError` if the Worker does not exist.

  ## Examples

      iex> get_worker!(123)
      %Worker{}

      iex> get_worker!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_worker!(integer()) :: Worker.t() | no_return()
  def get_worker!(id), do: Repo.get!(Worker, id)

  @spec acquire_workers_for_session(Scenario.Session.t()) :: {integer(), [Worker.t()]}
  def acquire_workers_for_session(%Session{id: id, nodes: nodes}) do
    {:ok, %{update: result}} =
      Ecto.Multi.new()
      |> Ecto.Multi.run(:workers_available, fn repo, _changes ->
        query =
          from w in Worker,
            where: w.status == :online,
            where: is_nil(w.session_id),
            limit: ^nodes

        {:ok, repo.all(query)}
      end)
      |> Ecto.Multi.update_all(
        :update,
        fn %{workers_available: workers_available} ->
          ids = Enum.map(workers_available, & &1.id)

          from(w in Worker,
            where: w.id in ^ids,
            select: w,
            update: [set: [status: :busy, session_id: ^id]]
          )
        end,
        []
      )
      |> Repo.transaction()

    result
  end

  @spec get_worker_by_nodename(atom()) :: Worker.t() | nil
  def get_worker_by_nodename(nodename) do
    [_app, ipv4] =
      nodename
      |> to_string()
      |> String.split("@")

    Repo.get_by(Worker, ipv4: ipv4)
  end

  def get_worker_by_remote_id(provider, id) do
    Repo.get_by(Worker, provider: provider, remote_id: id)
  end

  @doc """
  Creates a worker.

  ## Examples

      iex> create_worker(%{field: value})
      {:ok, %Worker{}}

      iex> create_worker(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_worker(map()) :: {:ok, Worker.t()} | {:error, Ecto.Changeset.t()}
  def create_worker(attrs) do
    %Worker{}
    |> Worker.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a worker.

  ## Examples

      iex> update_worker(worker, %{field: new_value})
      {:ok, %Worker{}}

      iex> update_worker(worker, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_worker(Worker.t(), map()) :: {:ok, Worker.t()} | {:error, Ecto.Changeset.t()}
  def update_worker(%Worker{} = worker, attrs) do
    worker
    |> Worker.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a worker.

  ## Examples

      iex> delete_worker(worker)
      {:ok, %Worker{}}

      iex> delete_worker(worker)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_worker(Worker.t()) :: {:ok, Worker.t()} | {:error, Ecto.Changeset.t()}
  def delete_worker(%Worker{} = worker) do
    Repo.delete(worker)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking worker changes.

  ## Examples

      iex> change_worker(worker)
      %Ecto.Changeset{data: %Worker{}}

  """
  @spec change_worker(Worker.t(), map()) :: Ecto.Changeset.t()
  def change_worker(%Worker{} = worker, attrs \\ %{}) do
    Worker.changeset(worker, attrs)
  end

  alias DLT.Tests.Scenario.Session.Result

  @doc """
  Returns the list of scenario_session_results.

  ## Examples

      iex> list_scenario_session_results()
      [%Result{}, ...]

  """
  @spec list_scenario_session_results :: [Scenario.Session.Result.t()]
  def list_scenario_session_results do
    Repo.all(Result)
  end

  @doc """
  Gets a single result.

  Raises `Ecto.NoResultsError` if the Result does not exist.

  ## Examples

      iex> get_result!(123)
      %Result{}

      iex> get_result!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_result!(integer) :: Scenario.Session.Result.t() | no_return()
  def get_result!(id), do: Repo.get!(Result, id)

  @doc """
  Creates a result.

  ## Examples

      iex> create_result(%{field: value})
      {:ok, %Result{}}

      iex> create_result(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_result(map()) :: {:ok, Scenario.Session.Result.t()} | {:error, Ecto.Changeset.t()}
  def create_result(attrs \\ %{}) do
    %Result{}
    |> Result.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a result.

  ## Examples

      iex> update_result(result, %{field: new_value})
      {:ok, %Result{}}

      iex> update_result(result, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_result(Scenario.Session.Result.t(), map()) ::
          {:ok, Scenario.Session.Result.t()} | {:error, Ecto.Changeset.t()}
  def update_result(%Result{} = result, attrs) do
    result
    |> Result.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a result.

  ## Examples

      iex> delete_result(result)
      {:ok, %Result{}}

      iex> delete_result(result)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_result(Scenario.Session.Result.t()) ::
          {:ok, Scenario.Session.Result.t()} | {:error, Ecto.Changeset.t()}
  def delete_result(%Result{} = result) do
    Repo.delete(result)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking result changes.

  ## Examples

      iex> change_result(result)
      %Ecto.Changeset{data: %Result{}}

  """
  @spec change_result(Scenario.Session.Result.t(), map()) :: Ecto.Changeset.t()
  def change_result(%Result{} = result, attrs \\ %{}) do
    Result.changeset(result, attrs)
  end

  def list_locations do
    Repo.all(DLT.Tests.Location)
  end
end
