defmodule DLT.Tests.Worker do
  use Ecto.Schema
  import Ecto.Changeset

  alias DLT.Tests

  @type t :: %__MODULE__{}

  schema "workers" do
    field :ipv4, :string
    field :password, :string
    field :provider, Ecto.Enum, values: [:none, :hetzner, :vultr], default: :none
    field :remote_id, :string
    field :status, Ecto.Enum, values: [:initializing, :online, :busy], default: :initializing

    belongs_to :session, Tests.Scenario.Session

    timestamps()
  end

  @doc false
  def changeset(worker, attrs) do
    worker
    |> cast(attrs, [:provider, :status, :remote_id, :ipv4, :password, :session_id])
    |> validate_required([:provider, :status])
  end
end
