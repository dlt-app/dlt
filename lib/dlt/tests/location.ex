defmodule DLT.Tests.Location do
  use Ecto.Schema
  import Ecto.Changeset

  # alias DLT.Tests

  @type t :: %__MODULE__{}

  schema "locations" do
    field :country, :string
    field :remote_id, :string
    field :provider, Ecto.Enum, values: [:none, :hetzner, :vultr], default: :none

    # has_many :workers, Tests.Worker

    timestamps()
  end

  @doc false
  def changeset(worker, attrs) do
    worker
    |> cast(attrs, [:country, :remote_id, :provider])
    |> validate_required([:country, :remote_id, :provider])
  end
end
