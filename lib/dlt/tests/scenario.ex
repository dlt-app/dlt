defmodule DLT.Tests.Scenario do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__
  alias DLT.Tests

  @type t :: %__MODULE__{}

  schema "scenarios" do
    field :duration, :integer
    field :name, :string
    field :nodes, :integer
    field :rps, :integer

    has_many :requests, Scenario.Request, on_replace: :delete
    has_many :sessions, Scenario.Session

    has_many :scenario_locations, Tests.ScenarioLocation
    has_many :locations, through: [:scenario_locations, :location]

    timestamps()
  end

  @doc false
  def changeset(scenario, attrs) do
    scenario
    |> cast(attrs, [:name, :nodes, :duration, :rps])
    |> cast_assoc(:requests)
    |> cast_assoc(:scenario_locations)
    |> validate_required([:name, :nodes, :duration, :rps])
  end
end
