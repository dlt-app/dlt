defmodule DLT.Tests.ScenarioLocation do
  use Ecto.Schema

  import Ecto.Changeset

  alias DLT.Tests

  schema "scenarios_locations" do
    field :number, :integer

    belongs_to :scenario, Tests.Scenario
    belongs_to :location, Tests.Location

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:number])
    |> validate_required([:number])
  end
end
