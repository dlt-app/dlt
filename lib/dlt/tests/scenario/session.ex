defmodule DLT.Tests.Scenario.Session do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__
  alias DLT.Tests

  @type t :: %__MODULE__{}

  schema "scenario_sessions" do
    field :duration, :integer
    field :finished_at, :naive_datetime
    field :nodes, :integer
    field :rps, :integer
    field :started_at, :naive_datetime
    field :status, Ecto.Enum, values: [:starting, :running, :finished], default: :starting

    embeds_many :requests, Session.Request

    belongs_to :scenario, Tests.Scenario
    has_many :workers, Tests.Worker
    has_many :results, Session.Result

    timestamps()
  end

  @doc false
  def changeset(session, attrs) do
    session
    |> cast(attrs, [:nodes, :duration, :rps, :status, :started_at, :finished_at, :scenario_id])
    |> maybe_put_requests(attrs)
    |> validate_required([:nodes, :duration, :rps, :status, :scenario_id])
  end

  # requests are read-only and are to be passed on Session creation only
  defp maybe_put_requests(changeset, %{"requests" => requests}) do
    requests =
      Enum.map(requests, fn %Tests.Scenario.Request{} = request ->
        Session.Request.changeset(%{
          "url" => request.url,
          "method" => request.method,
          "headers" => request.headers,
          "body" => request.body
        })
      end)

    put_embed(changeset, :requests, requests)
  end

  defp maybe_put_requests(changeset, _attrs), do: changeset
end
