defmodule DLT.Tests.Scenario.Session.Result do
  use Ecto.Schema
  import Ecto.Changeset

  alias DLT.Tests

  @type t :: %__MODULE__{}

  schema "scenario_session_results" do
    field :second, :integer
    field :code, :string
    field :mean, :integer
    field :count, :integer

    belongs_to :session, Tests.Scenario.Session
    belongs_to :worker, Tests.Worker

    timestamps()
  end

  @doc false
  def changeset(result, attrs) do
    result
    |> cast(attrs, [:second, :code, :mean, :count, :session_id, :worker_id])
    |> validate_required([:second, :code, :mean, :count, :session_id, :worker_id])
  end
end
