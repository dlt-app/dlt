defmodule DLT.Tests.Scenario.Session.Manager do
  use GenServer, restart: :transient

  require Logger

  alias DLT.Tests

  # @default_provider :vultr

  def start_link(scenario) do
    name = scenario_to_manager_name(scenario)
    GenServer.start_link(__MODULE__, scenario, name: name)
  end

  def init(scenario) do
    {:ok, session} = Tests.create_session_from_scenario(scenario)
    Logger.debug("Initializing session manager for session #{session.id}...")

    Phoenix.PubSub.subscribe(DLT.PubSub, "session:#{session.id}")

    send(self(), :create_workers)

    {:ok, %{session: session, workers: []}}
  end

  def handle_info(:create_workers, %{session: session} = state) do
    {available_workers_count, available_workers} = Tests.acquire_workers_for_session(session)

    to_create =
      if available_workers_count < session.nodes do
        1..(session.nodes - length(available_workers))
      else
        []
      end

    workers =
      Enum.reduce(to_create, available_workers, fn i, workers ->
        provider =
          if rem(i, 2) == 0 do
            :vultr
          else
            :hetzner
          end

        {:ok, worker} =
          Tests.create_worker(%{
            "session_id" => session.id,
            "status" => :initializing,
            "provider" => provider
          })

        provider = Module.concat(DLT.Cloud.Provider, String.capitalize("#{worker.provider}"))
        {:ok, new_worker} = provider.create_server(worker)
        [new_worker | workers]
      end)

    if to_create == [] do
      send(self(), :start)
    end

    {:noreply, %{state | workers: workers}}
  end

  def handle_info({:worker_connected, new_worker}, state) do
    Logger.debug("Worker #{new_worker.ipv4} is ready for the session #{state.session.id}")

    new_workers =
      Enum.map(state.workers, fn worker ->
        # can not compare by ipv4 because it can be unassigned for `worker`
        if {worker.provider, worker.remote_id} == {new_worker.provider, new_worker.remote_id} do
          {:ok, new_worker} = Tests.update_worker(new_worker, %{"status" => :busy})

          new_worker
        else
          worker
        end
      end)

    if Enum.all?(new_workers, &(&1.status != :initializing)) do
      Logger.debug("Session #{state.session.id} is ready to start.")
      send(self(), :start)
    else
      workers_left = length(Enum.filter(new_workers, &(&1.status == :initializing)))
      Logger.debug("#{workers_left} more workers left for session #{state.session.id}")
    end

    {:noreply, %{state | workers: new_workers}}
  end

  def handle_info(:start, %{session: session, workers: workers} = state) do
    Logger.debug("Running the session #{session.id}")

    requests = Enum.map(session.requests, &Map.take(&1, [:url, :method, :headers, :body]))

    params = %{
      requests: requests,
      rps: session.rps,
      duration: session.duration
    }

    load = trunc(session.rps / length(workers))
    add = rem(session.rps, length(workers))
    [%{id: first_worker_id} | _] = workers

    Enum.each(workers, fn worker ->
      prefix = "dlt-worker-#{worker.provider}"
      node_name = String.to_atom("#{prefix}@#{worker.ipv4}")

      rps =
        if worker.id == first_worker_id do
          load + add
        else
          load
        end

      run_params = %{params | rps: rps}

      Logger.debug(
        "Calling worker #{worker.id} for session #{session.id}:\n:rpc.call(#{inspect(node_name)}, DLTWorker.Server, :run, [#{inspect(run_params)}, #{inspect(self())}])"
      )

      :rpc.call(node_name, DLTWorker.Server, :run, [run_params, self()])
    end)

    {:ok, updated_session} =
      Tests.update_session(session, %{status: :running, started_at: NaiveDateTime.utc_now()})

    {:noreply, %{state | session: updated_session}}
  end

  def handle_info({:result, second, from, data}, %{session: %{duration: second}} = state) do
    ip = from |> to_string() |> String.split("@") |> Enum.at(1)

    new_workers =
      Enum.map(state.workers, fn worker ->
        if worker.ipv4 == ip do
          {:ok, new_worker} = Tests.update_worker(worker, %{status: :online, session_id: nil})
          new_worker
        else
          worker
        end
      end)

    write_results(second, from, data, state.session)

    new_state = %{state | workers: new_workers}

    if Enum.all?(new_workers, &(&1.status != :busy)) do
      {:stop, :normal, new_state}
    else
      {:noreply, new_state}
    end
  end

  def handle_info({:result, second, from, data}, state) do
    write_results(second, from, data, state.session)

    {:noreply, state}
  end

  def terminate(:normal, state) do
    Logger.debug("Terminating session manager for session #{state.session.id}...")

    {:ok, _updated_session} =
      Tests.update_session(state.session, %{finished_at: DateTime.utc_now(), status: :finished})

    :ok
  end

  def scenario_to_manager_name(scenario) do
    "#{__MODULE__}#{scenario.id}"
    |> String.to_atom()
  end

  defp write_results(second, nodename, data, session) do
    Enum.each(data, fn {code, result_data} ->
      Tests.create_result(%{
        second: second,
        code: to_string(code),
        mean: result_data.mean |> Float.round() |> trunc(),
        count: result_data.count,
        session_id: session.id,
        worker_id: Tests.get_worker_by_nodename(nodename).id
      })
    end)
  end
end
