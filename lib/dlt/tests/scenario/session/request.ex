defmodule DLT.Tests.Scenario.Session.Request do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{}

  embedded_schema do
    field :body, :string
    field :headers, :string
    field :method, Ecto.Enum, values: [:get, :post], default: :get
    field :url, :string
  end

  @doc false
  def changeset(request \\ %__MODULE__{}, attrs) do
    request
    |> cast(attrs, [:method, :url, :headers, :body])
    |> validate_required([:method, :url])
  end
end
