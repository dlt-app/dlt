defmodule DLT.Tests.Scenario.Request do
  use Ecto.Schema
  import Ecto.Changeset

  alias DLT.Tests

  @type t :: %__MODULE__{}

  schema "scenario_requests" do
    field :body, :string
    field :headers, :string
    field :method, Ecto.Enum, values: [:get, :post], default: :get
    field :url, :string

    field :delete, :boolean, virtual: true

    belongs_to :scenario, Tests.Scenario

    timestamps()
  end

  @doc false
  def changeset(request, attrs) do
    request
    |> cast(attrs, [:method, :url, :body, :headers, :delete, :scenario_id])
    |> validate_required([:method, :url])
    |> maybe_mark_for_deletion()
  end

  defp maybe_mark_for_deletion(changeset) do
    if get_change(changeset, :delete) do
      %{changeset | action: :delete}
    else
      changeset
    end
  end
end
