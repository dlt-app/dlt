defmodule DLT.Cloud.Locations do
  use GenServer

  # update regions every minute
  @update_interval 60_000

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  def list_locations do
    GenServer.call(__MODULE__, :list_regions)
  end

  def init(_opts) do
    Process.send_after(self(), :update, @update_interval)
    {:ok, %{}}
  end

  def handle_call(:list_locations, state) do
    {:reply, state, state}
  end

  def handle_info(:update, _state) do
    providers = get_provider_modules()

    locations =
      Enum.reduce(providers, %{}, fn provider, acc ->
        provider_locations = provider.list_locations()

        Enum.reduce(provider_locations, acc, fn %{country: country, id: id}, acc2 ->
          provider_name =
            provider
            |> Module.split()
            |> List.last()
            |> String.downcase()
            |> String.to_atom()

          data = %{provider: provider_name, id: id}

          Map.update(acc2, country, [data], &[data | &1])
        end)
      end)

    {:noreply, locations}
  end

  defp get_provider_modules do
    :code.all_loaded()
    |> Enum.filter(fn {name, _beam_path} ->
      name
      |> to_string()
      |> String.starts_with?("Elixir.DLT.Cloud.Provider")
    end)
    |> Enum.map(fn {name, _beam_path} -> name end)
  end
end
