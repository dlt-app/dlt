defmodule DLT.Cloud.Provider.Vultr do
  use Tesla
  use GenServer

  require Logger

  alias DLT.Tests

  @api_token Application.compile_env(:dlt, [:providers, :vultr, :api_token])

  @label "dlt-worker"
  @script_name "dlt-worker-startup-script"

  plug {Tesla.Middleware.Headers, [{"Authorization", "Bearer #{@api_token}"}]}
  plug Tesla.Middleware.JSON

  @api_endpoint "https://api.vultr.com/v2"

  def create_server(%Tests.Worker{} = worker) do
    {:ok, script_id} = get_startup_script_id()

    url = "#{@api_endpoint}/instances"

    node_name = "dlt-worker-vultr"

    user_data =
      %{"name" => node_name, "cookie" => Node.get_cookie()}
      |> Jason.encode!()
      |> Base.encode64()

    body = %{
      "hostname" => "#{node_name}-#{worker.id}",
      "script_id" => script_id,
      "user_data" => user_data,
      "plan" => "vc2-1c-1gb",
      "os_id" => "387",
      "region" => "fra",
      "label" => @label
    }

    {:ok, %{body: response, status: status}} = post(url, body)

    if status in 200..204 do
      instance = response["instance"]

      {:ok, new_worker} =
        Tests.update_worker(worker, %{
          "remote_id" => to_string(instance["id"]),
          # ipv4 will be 0.0.0.0 at the moment of creation
          "ipv4" => instance["main_ip"],
          "password" => instance["default_password"]
        })

      {:ok, new_worker}
    else
      Logger.error(
        "Vultr.create_server/1 Vultr responded with code #{status}:\n#{inspect(response)}"
      )

      {:error, {status, response}}
    end
  end

  def list_servers do
    url = "#{@api_endpoint}/instances"

    {:ok, %{body: response, status: status}} = get(url, query: [label: @label, per_page: 500])

    if status == 200 do
      {:ok, response["instances"]}
    else
      {:error, {status, response}}
    end
  end

  def delete_server(%Tests.Worker{} = worker) do
    "dlt@#{worker.ipv4}"
    |> String.to_atom()
    |> Node.disconnect()

    url = "#{@api_endpoint}/instances/#{worker.remote_id}"
    {:ok, %{body: response, status: status}} = delete(url)

    if status in 200..204 do
      Tests.delete_worker(worker)
    else
      Logger.error("Error deleting worker #{worker.id}: #{inspect(response)}")
      {:error, response}
    end
  end

  defp get_startup_script_id do
    url = "#{@api_endpoint}/startup-scripts"
    {:ok, %{body: response, status: _status}} = get(url)

    existing_script = Enum.find(response["startup_scripts"], &(&1["name"] == @script_name))

    if existing_script != nil do
      Logger.debug("Startup script already exists")
      {:ok, existing_script["id"]}
    else
      script =
        :dlt
        |> Application.app_dir("priv/vultr-startup-script.sh")
        |> File.read!()
        |> Base.encode64()

      body = %{
        "name" => @script_name,
        "script" => script
      }

      {:ok, %{body: response, status: status}} = post(url, body)

      if status in 200..204 do
        {:ok, response["startup_script"]["id"]}
      else
        Logger.error(
          "Vultr.get_startup_script_id/0 unexpected response with status code #{status} from Vultr:\n#{inspect(response)}"
        )

        {:error, {status, response}}
      end
    end
  end

  def start_link(args) do
    GenServer.start_link(__MODULE__, [args], name: __MODULE__)
  end

  def init(_args) do
    Process.send_after(self(), :ping, 1000)
    {:ok, []}
  end

  def handle_cast({:awaiting_ip, worker}, state) do
    {:noreply, [worker | state]}
  end

  def handle_info(:ping, []) do
    Process.send_after(self(), :ping, 5000)
    {:noreply, []}
  end

  def handle_info(:ping, workers) do
    servers =
      list_servers()
      |> Enum.map(&%{id: &1["id"], ip: &1["main_ip"]})

    new_workers =
      workers
      |> Enum.map(fn worker ->
        found = Enum.find(servers, &(&1.id == worker.remote_id))

        if found do
          {:ok, _new_worker} = Tests.update_worker(worker, %{ipv4: found.ip})
          nil
        else
          worker
        end
      end)
      |> Enum.reject(&is_nil/1)

    Process.send_after(self(), :ping, 5000)
    {:noreply, new_workers}
  end

  def list_locations do
    url = "#{@api_endpoint}/regions"

    {:ok, %{body: response, status: status}} = get(url)

    if status == 200 do
      locations = Enum.map(response["regions"], &%{country: &1["country"], id: &1["id"]})
      {:ok, locations}
    else
      {:error, {status, response}}
    end
  end
end
