defmodule DLT.Cloud.Provider.Hetzner do
  use Tesla

  require Logger

  alias DLT.Tests

  @api_token Application.compile_env(:dlt, [:providers, :hetzner, :api_token])

  @labels %{"dlt" => "worker"}

  plug {Tesla.Middleware.Headers, [{"Authorization", "Bearer #{@api_token}"}]}
  plug Tesla.Middleware.JSON

  @api_endpoint "https://api.hetzner.cloud/v1"

  def create_server(%Tests.Worker{} = worker) do
    url = "#{@api_endpoint}/servers"

    node_name = "dlt-worker-hetzner"
    vars = [name: node_name, cookie: Node.get_cookie()]
    user_data = EEx.eval_file(Application.app_dir(:dlt, "priv/cloud-init-config.yml"), vars)

    body = %{
      "name" => "#{node_name}-#{worker.id}",
      "server_type" => "cx11",
      "image" => "ubuntu-20.04",
      "user_data" => user_data,
      "labels" => @labels
    }

    {:ok, %{body: response, status: status}} = post(url, body)

    if status == 201 do
      remote_id = get_in(response, ["server", "id"])
      password = response["root_password"]
      ip = get_in(response, ["server", "public_net", "ipv4", "ip"])

      {:ok, new_worker} =
        Tests.update_worker(worker, %{
          "remote_id" => to_string(remote_id),
          "ipv4" => ip,
          "password" => password
        })

      {:ok, new_worker}
    else
      Logger.error(
        "Hetzner.create_server/1 Hetzner responded with code #{status}:\n#{inspect(response)}"
      )

      {:error, {status, response}}
    end
  end

  @doc """
  Fetch 50 servers from Hetzner with labels specified in `@labels`.
  TODO: add pagination
  """
  def list_servers do
    url = "#{@api_endpoint}/servers"

    labels =
      @labels
      |> Enum.map(fn {key, value} -> "#{key}=#{value}" end)
      |> Enum.join(",")

    {:ok, %{body: response, status: status}} =
      get(url, query: [label_selector: labels, per_page: 50])

    if status == 200 do
      {:ok, response["servers"]}
    else
      {:error, {status, response}}
    end
  end

  def delete_server(%Tests.Worker{} = worker) do
    "dlt@#{worker.ipv4}"
    |> String.to_atom()
    |> Node.disconnect()

    url = "#{@api_endpoint}/servers/#{worker.remote_id}"
    {:ok, %{body: response, status: _status}} = delete(url)

    case response["action"]["error"] do
      nil ->
        Tests.delete_worker(worker)

      err ->
        Logger.error("Error deleting worker #{worker.id}: #{inspect(err)}")
        {:error, err}
    end
  end

  def list_locations do
    url = "#{@api_endpoint}/locations"

    {:ok, %{body: response, status: status}} = get(url)

    if status == 200 do
      locations =
        Enum.map(response["locations"], &%{country: &1["country"], id: to_string(&1["id"])})

      {:ok, locations}
    else
      {:error, {status, response}}
    end
  end
end
