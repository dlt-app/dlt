defmodule DLT.Repo do
  use Ecto.Repo,
    otp_app: :dlt,
    adapter: Ecto.Adapters.Postgres
end
