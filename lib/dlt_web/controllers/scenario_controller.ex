defmodule DLTWeb.ScenarioController do
  use DLTWeb, :controller

  alias DLT.Tests
  alias DLT.Tests.Scenario
  alias DLT.Tests.Scenario.Session

  def index(conn, _params) do
    scenarios = Tests.list_scenarios()
    render(conn, "index.html", scenarios: scenarios)
  end

  def new(conn, _params) do
    locations = Tests.list_locations()
    changeset = Tests.change_scenario(%Scenario{requests: [%Scenario.Request{}]})
    render(conn, "new.html", changeset: changeset, locations: locations)
  end

  def create(conn, %{"scenario" => scenario_params}) do
    requests =
      scenario_params["requests"]
      |> Enum.map(fn {_index, map} -> map end)
      |> Enum.reject(& &1["delete"])

    scenario_params = Map.put(scenario_params, "requests", requests)

    case Tests.create_scenario(scenario_params) do
      {:ok, scenario} ->
        conn
        |> put_flash(:info, "Scenario created successfully.")
        |> redirect(to: Routes.scenario_path(conn, :show, scenario))

      {:error, %Ecto.Changeset{} = changeset} ->
        locations = Tests.list_locations()
        render(conn, "new.html", changeset: changeset, locations: locations)
    end
  end

  def show(conn, %{"id" => id}) do
    scenario = Tests.get_scenario!(id)

    # session to show is either the one currently running, or the latest one
    session_to_show =
      with list when list != [] <- scenario.sessions,
           nil <- Enum.find(list, &(&1.status != :finished)) do
        scenario.sessions
        |> Enum.sort(&(NaiveDateTime.compare(&1.started_at, &2.started_at) == :gt))
        |> hd()
      else
        [] -> nil
        session -> session
      end

    render(conn, "show.html", scenario: scenario, session: session_to_show)
  end

  def edit(conn, %{"id" => id}) do
    locations = Tests.list_locations()
    scenario = Tests.get_scenario!(id)
    changeset = Tests.change_scenario(scenario)
    render(conn, "edit.html", scenario: scenario, changeset: changeset, locations: locations)
  end

  def update(conn, %{"id" => id, "scenario" => scenario_params}) do
    requests =
      scenario_params["requests"]
      |> Enum.map(fn {_index, map} -> map end)
      |> Enum.reject(&(&1["delete"] == "true" and is_nil(&1["id"])))

    scenario_params = Map.put(scenario_params, "requests", requests)

    scenario = Tests.get_scenario!(id)

    case Tests.update_scenario(scenario, scenario_params) do
      {:ok, scenario} ->
        conn
        |> put_flash(:info, "Scenario updated successfully.")
        |> redirect(to: Routes.scenario_path(conn, :show, scenario))

      {:error, %Ecto.Changeset{} = changeset} ->
        locations = Tests.list_locations()
        render(conn, "edit.html", scenario: scenario, changeset: changeset, locations: locations)
    end
  end

  def delete(conn, %{"id" => id}) do
    scenario = Tests.get_scenario!(id)
    {:ok, _scenario} = Tests.delete_scenario(scenario)

    conn
    |> put_flash(:info, "Scenario deleted successfully.")
    |> redirect(to: Routes.scenario_path(conn, :index))
  end

  def start_session(conn, %{"id" => id}) do
    scenario = Tests.get_scenario!(id)

    {:ok, _child} =
      DynamicSupervisor.start_child(Session.ManagerSupervisor, {Session.Manager, scenario})

    redirect(conn, to: Routes.scenario_path(conn, :index))
  end
end
