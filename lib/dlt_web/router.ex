defmodule DLTWeb.Router do
  use DLTWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", DLTWeb do
    pipe_through :browser

    get "/", ScenarioController, :index

    scope "/scenarios" do
      resources "/", ScenarioController
      post "/start_session", ScenarioController, :start_session
    end

    resources "/workers", WorkerController
  end

  import Phoenix.LiveDashboard.Router

  scope "/" do
    pipe_through :browser
    live_dashboard "/dashboard", metrics: DLTWeb.Telemetry
  end
end
