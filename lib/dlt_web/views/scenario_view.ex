defmodule DLTWeb.ScenarioView do
  use DLTWeb, :view

  def has_session_started?(scenario) do
    Enum.any?(scenario.sessions, &(&1.status in [:starting, :running]))
  end

  def session_results(session) do
    session.results
    |> Enum.reduce(%{}, fn result, acc ->
      new_result = %{mean: result.mean, count: result.count}

      Map.update(acc, result.second, [new_result], fn results ->
        [new_result | results]
      end)
    end)
    |> Enum.map(fn {second, results} ->
      total_count = Enum.map(results, & &1.count) |> Enum.sum()
      sum_of_averages = Enum.map(results, &(&1.count * &1.mean)) |> Enum.sum()
      grand_average = sum_of_averages / total_count
      {second, %{mean: grand_average, count: total_count}}
    end)
    |> Enum.into(%{})
  end

  def session_grand_average_response_time(results) do
    results = Enum.map(results, fn {_second, data} -> data end)
    total_count = Enum.map(results, & &1.count) |> Enum.sum()
    sum_of_averages = Enum.map(results, &(&1.count * &1.mean)) |> Enum.sum()
    sum_of_averages / total_count
  end

  def responses_count_by_codes(session) do
    Enum.reduce(session.results, %{success: 0, timeout: 0, error: 0, network: 0}, fn result,
                                                                                     acc ->
      case result.code do
        "200" ->
          Map.update!(acc, :success, &(&1 + result.count))

        err when err in ["500"] ->
          Map.update!(acc, :error, &(&1 + result.count))

        "502" ->
          Map.update!(acc, :network, &(&1 + result.count))

        "504" ->
          Map.update!(acc, :timeout, &(&1 + result.count))

        unknown ->
          IO.inspect({:unknown_code, unknown})
          Map.update!(acc, :error, &(&1 + result.count))
      end
    end)
  end
end
