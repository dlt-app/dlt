#!/bin/bash

# Add erlang-solutions apt source
wget https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb
dpkg -i erlang-solutions_2.0_all.deb

# Safely run `apt update` and `apt full-upgrade` considering apt locks
# Source: https://www.vultr.com/docs/automating-ubuntu-16-updates-with-vultr-startup-scripts
UPGRADE_ATTEMPT_COUNT=100
UPGRADE_STATE=1
for i in `seq 1 $UPGRADE_ATTEMPT_COUNT`;
do
    if [ "$UPGRADE_STATE" -eq "1" ]; then
        apt -y update
        if [ "`echo $?`" -eq "0" ]; then
            echo "package list updated."
            # UPGRADE_STATE=2;
            UPGRADE_STATE=3;
        fi
    fi

    if [ "$UPGRADE_STATE" -eq "2" ]; then
        apt -y full-upgrade
        if [ "`echo $?`" -eq "0" ]; then
            echo "packages updated."
            UPGRADE_STATE=3;
        fi
    fi

    if [ "$UPGRADE_STATE" -eq "3" ]; then
        break
    fi

    sleep 5
done

if [ "$UPGRADE_STATE" -ne "3" ]; then
    echo "ERROR: packages failed to update after $UPGRADE_ATTEMPT_COUNT attempts."
fi

export HOME=/root

apt -y install elixir git curl jq

git clone "https://gitlab.com/dlt-app/worker.git" /var/dlt-worker
cd /var/dlt-worker
mix local.hex --force
mix local.rebar --force
mix deps.get
mix compile

cp priv/dlt-worker.service.tpl /etc/systemd/system/dlt-worker.service

IP=$(curl http://169.254.169.254/v1/interfaces/0/ipv4/address)
UD=$(curl http://169.254.169.254/latest/user-data)
NODE_NAME=$(echo $UD | jq -r .name)
COOKIE=$(echo $UD | jq -r .cookie)

sed -i "s/\${IP}/$IP/g" /etc/systemd/system/dlt-worker.service
sed -i "s/\${NODE_NAME}/$NODE_NAME/g" /etc/systemd/system/dlt-worker.service
sed -i "s/\${COOKIE}/$COOKIE/g" /etc/systemd/system/dlt-worker.service

echo "Starting DLT-worker service"
systemctl start dlt-worker
echo "DLT-worker service status:"
systemctl is-active dlt-worker