defmodule DLT.Repo.Migrations.CreateWorkers do
  use Ecto.Migration

  def change do
    DLT.Ecto.Migration.Enum.create_type("provider", ~w(none hetzner vultr))
    DLT.Ecto.Migration.Enum.create_type("worker_status", ~w(initializing online busy))

    create table(:workers) do
      add :provider, :provider
      add :status, :worker_status
      add :remote_id, :string
      add :ipv4, :string
      add :password, :string
      add :session_id, references(:scenario_sessions, on_delete: :nothing)

      timestamps()
    end

    create index(:workers, [:session_id])
  end
end
