defmodule DLT.Repo.Migrations.CreateScenarioRequests do
  use Ecto.Migration

  def change do
    DLT.Ecto.Migration.Enum.create_type("http_method", ~w(head get delete trace options post put patch))

    create table(:scenario_requests) do
      add :method, :http_method
      add :url, :string
      add :body, :text
      add :headers, :text
      add :scenario_id, references(:scenarios, on_delete: :delete_all)

      timestamps()
    end

    create index(:scenario_requests, [:scenario_id])
  end
end
