defmodule DLT.Repo.Migrations.CreateLocations do
  use Ecto.Migration

  def change do
    create table(:locations) do
      add :country, :string
      add :remote_id, :string
      add :provider, :provider

      timestamps()
    end
  end
end
