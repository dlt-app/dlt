defmodule DLT.Repo.Migrations.CreateScenarioSessionResults do
  use Ecto.Migration

  def change do
    create table(:scenario_session_results) do
      add :second, :integer
      add :code, :string
      add :mean, :integer
      add :count, :integer
      add :session_id, references(:scenario_sessions, on_delete: :delete_all)
      add :worker_id, references(:workers, on_delete: :nilify_all)

      timestamps()
    end

    create index(:scenario_session_results, [:session_id])
    create index(:scenario_session_results, [:worker_id])
  end
end
