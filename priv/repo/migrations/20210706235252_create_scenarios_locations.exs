defmodule DLT.Repo.Migrations.CreateScenariosLocations do
  use Ecto.Migration

  def change do
    create table(:scenarios_locations) do
      add :scenario_id, references(:scenarios)
      add :location_id, references(:locations)
      add :number, :integer

      timestamps()
    end
  end
end
