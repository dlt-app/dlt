defmodule DLT.Repo.Migrations.CreateScenarios do
  use Ecto.Migration

  def change do
    create table(:scenarios) do
      add :name, :string
      add :nodes, :integer
      add :duration, :integer
      add :rps, :integer

      timestamps()
    end

  end
end
