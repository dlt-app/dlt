defmodule DLT.Repo.Migrations.CreateScenarioSessions do
  use Ecto.Migration

  def change do
    DLT.Ecto.Migration.Enum.create_type("scenario_session_status", ~w(starting running finished))

    create table(:scenario_sessions) do
      add :nodes, :integer
      add :duration, :integer
      add :rps, :integer
      add :status, :scenario_session_status
      add :started_at, :naive_datetime
      add :finished_at, :naive_datetime
      add :requests, {:array, :map}
      add :scenario_id, references(:scenarios, on_delete: :nilify_all)

      timestamps()
    end

    create index(:scenario_sessions, [:scenario_id])
  end
end
