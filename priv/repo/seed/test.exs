# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     DLT.Repo.insert!(%DLT.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

providers = [:vultr, :hetzner]

Enum.each(providers, fn provider ->
  locations = [%{country: "RU", id: "1"}, %{country: "UK", id: "2"}]

  Enum.each(locations, fn %{country: country, id: id} ->
    DLT.Repo.insert!(%DLT.Tests.Location{
      country: country,
      remote_id: id,
      provider: provider
    })
  end)
end)
