# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     DLT.Repo.insert!(%DLT.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

if Mix.env() == :test do
  Application.put_env(:tesla, DLT.Cloud.Provider.Hetzner, adapter: DLT.Cloud.Provider.Hetzner)
  Application.put_env(:tesla, DLT.Cloud.Provider.Vultr, adapter: DLT.Cloud.Provider.Vultr)
end

providers = [:vultr, :hetzner]

Enum.each(providers, fn provider ->
  module =
    "Elixir.DLT.Cloud.Provider.#{provider |> to_string() |> String.capitalize()}"
    |> String.to_atom()

  {:ok, locations} = module.list_locations()

  Enum.each(locations, fn %{country: country, id: id} ->
    DLT.Repo.insert!(%DLT.Tests.Location{
      country: country,
      remote_id: id,
      provider: provider
    })
  end)
end)
