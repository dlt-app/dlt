defmodule DLT.TestsTest do
  use DLT.DataCase, async: true

  alias DLT.Tests

  describe "scenarios" do
    alias DLT.Tests.Scenario

    @valid_attrs %{duration: 42, name: "some name", nodes: 42, rps: 42}
    @update_attrs %{duration: 43, name: "some updated name", nodes: 43, rps: 43}
    @invalid_attrs %{duration: nil, name: nil, nodes: nil, rps: nil}

    test "list_scenarios/0 returns all scenarios" do
      fixture = insert(:scenario)

      scenarios =
        Tests.list_scenarios()
        |> Enum.map(&Map.drop(&1, [:requests, :sessions, :scenario_locations]))

      expected = Map.drop(fixture, [:requests, :sessions, :scenario_locations])
      assert scenarios == [expected]
    end

    test "get_scenario!/1 returns the scenario with given id" do
      fixture = insert(:scenario)
      scenario = Tests.get_scenario!(fixture.id)

      assert Map.drop(fixture, [:requests, :sessions, :scenario_locations]) ==
               Map.drop(scenario, [:requests, :sessions, :scenario_locations])
    end

    test "create_scenario/1 with valid data creates a scenario" do
      assert {:ok, %Scenario{} = scenario} = Tests.create_scenario(@valid_attrs)
      assert scenario.duration == 42
      assert scenario.name == "some name"
      assert scenario.nodes == 42
      assert scenario.rps == 42
    end

    test "create_scenario/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tests.create_scenario(@invalid_attrs)
    end

    test "update_scenario/2 with valid data updates the scenario" do
      scenario = insert(:scenario)
      assert {:ok, %Scenario{} = scenario} = Tests.update_scenario(scenario, @update_attrs)
      assert scenario.duration == 43
      assert scenario.name == "some updated name"
      assert scenario.nodes == 43
      assert scenario.rps == 43
    end

    test "update_scenario/2 with invalid data returns error changeset" do
      scenario_fixture = insert(:scenario)
      assert {:error, %Ecto.Changeset{}} = Tests.update_scenario(scenario_fixture, @invalid_attrs)

      scenario = Tests.get_scenario!(scenario_fixture.id)

      assert Map.drop(scenario_fixture, [:requests, :sessions, :scenario_locations]) ==
               Map.drop(scenario, [:requests, :sessions, :scenario_locations])
    end

    test "delete_scenario/1 deletes the scenario" do
      scenario = insert(:scenario)
      assert {:ok, %Scenario{}} = Tests.delete_scenario(scenario)
      assert_raise Ecto.NoResultsError, fn -> Tests.get_scenario!(scenario.id) end
    end

    test "change_scenario/1 returns a scenario changeset" do
      scenario = insert(:scenario)
      assert %Ecto.Changeset{} = Tests.change_scenario(scenario)
    end
  end

  describe "scenario_requests" do
    alias DLT.Tests.Scenario.Request

    @valid_attrs %{
      body: "some body",
      headers: "header1: value1",
      method: :post,
      url: "some url"
    }
    @update_attrs %{
      body: "some updated body",
      headers: "header1: value2",
      method: :get,
      url: "some updated url"
    }
    @invalid_attrs %{body: nil, headers: nil, method: nil, url: nil}

    test "list_scenario_requests/0 returns all scenario_requests" do
      fixture_scenario = insert(:scenario)
      fixture_requests = fixture_scenario.requests

      requests = Tests.list_scenario_requests() |> Enum.map(&Map.delete(&1, :scenario))
      expected = Enum.map(fixture_requests, &Map.delete(&1, :scenario))
      assert requests == expected
    end

    test "get_request!/1 returns the request with given id" do
      fixture = insert(:scenario_request)
      request = Tests.get_request!(fixture.id)

      assert Map.delete(fixture, :scenario) == Map.delete(request, :scenario)
    end

    test "create_request/1 with valid data creates a request" do
      scenario = insert(:scenario)
      params = Map.put_new(@valid_attrs, :scenario_id, scenario.id)
      assert {:ok, %Request{} = request} = Tests.create_request(params)
      assert request.body == "some body"
      assert request.headers == "header1: value1"
      assert request.method == :post
      assert request.url == "some url"
    end

    test "create_request/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tests.create_request(@invalid_attrs)
    end

    test "update_request/2 with valid data updates the request" do
      request = insert(:scenario_request)
      assert {:ok, %Request{} = request} = Tests.update_request(request, @update_attrs)
      assert request.body == "some updated body"
      assert request.headers == "header1: value2"
      assert request.method == :get
      assert request.url == "some updated url"
    end

    test "update_request/2 with invalid data returns error changeset" do
      fixture = insert(:scenario_request)
      assert {:error, %Ecto.Changeset{}} = Tests.update_request(fixture, @invalid_attrs)

      request = Tests.get_request!(fixture.id)

      assert Map.delete(fixture, :scenario) == Map.delete(request, :scenario)
    end

    test "delete_request/1 deletes the request" do
      request = insert(:scenario_request)
      assert {:ok, %Request{}} = Tests.delete_request(request)
      assert_raise Ecto.NoResultsError, fn -> Tests.get_request!(request.id) end
    end

    test "change_request/1 returns a request changeset" do
      request = insert(:scenario_request)
      assert %Ecto.Changeset{} = Tests.change_request(request)
    end
  end

  describe "scenario_sessions" do
    alias DLT.Tests.Scenario.Session

    @invalid_attrs %{
      duration: nil,
      finished_at: nil,
      nodes: nil,
      rps: nil,
      started_at: nil,
      status: nil
    }

    test "list_scenario_sessions/0 returns all scenario_sessions" do
      session_fixture = insert(:scenario_session)
      sessions = Tests.list_scenario_sessions() |> Enum.map(&Map.delete(&1, :scenario))
      expected = Map.delete(session_fixture, :scenario)
      assert sessions == [expected]
    end

    test "get_session!/1 returns the session with given id" do
      session_fixture = insert(:scenario_session)
      session = Tests.get_session!(session_fixture.id)
      assert Map.delete(session_fixture, :scenario) == Map.delete(session, :scenario)
    end

    test "create_session_from_scenario/1 with valid data creates a session" do
      scenario = insert(:scenario)
      assert {:ok, %Session{} = session} = Tests.create_session_from_scenario(scenario)
      assert session.duration == scenario.duration
      assert session.finished_at == nil
      assert session.nodes == scenario.nodes
      assert session.rps == scenario.rps
      assert session.started_at == nil
      assert session.status == :starting

      expected_requests =
        Enum.map(scenario.requests, fn req ->
          %{url: req.url, method: req.method, headers: req.headers, body: req.body}
        end)

      session_requests =
        Enum.map(session.requests, fn req ->
          %{url: req.url, method: req.method, headers: req.headers, body: req.body}
        end)

      assert session_requests == expected_requests
    end

    test "update_session/2 with valid data updates the session" do
      session_fixture = insert(:scenario_session)

      assert {:ok, %Session{} = session} =
               Tests.update_session(session_fixture, %{status: :running})

      assert session.status == :running
    end

    test "update_session/2 with invalid data returns error changeset" do
      session_fixture = insert(:scenario_session)
      assert {:error, %Ecto.Changeset{}} = Tests.update_session(session_fixture, @invalid_attrs)
      session = Tests.get_session!(session_fixture.id)

      assert Map.delete(session_fixture, :scenario) == Map.delete(session, :scenario)
    end

    test "delete_session/1 deletes the session" do
      session = insert(:scenario_session)
      assert {:ok, %Session{}} = Tests.delete_session(session)
      assert_raise Ecto.NoResultsError, fn -> Tests.get_session!(session.id) end
    end

    test "change_session/1 returns a session changeset" do
      session = insert(:scenario_session)
      assert %Ecto.Changeset{} = Tests.change_session(session)
    end
  end

  describe "workers" do
    alias DLT.Tests.Worker

    @valid_attrs %{
      ipv4: "some ipv4",
      password: "some password",
      provider: :none,
      remote_id: "some remote_id",
      status: :online
    }
    @update_attrs %{
      ipv4: "some updated ipv4",
      password: "some updated password",
      provider: :hetzner,
      remote_id: "some updated remote_id",
      status: :busy
    }
    @invalid_attrs %{ipv4: nil, password: nil, provider: nil, remote_id: nil, status: nil}

    test "list_workers/0 returns all workers" do
      worker_fixture = insert(:worker)
      workers = Tests.list_workers() |> Enum.map(&Map.delete(&1, :session))
      expected = Map.delete(worker_fixture, :session)
      assert workers == [expected]
    end

    test "acquire_workers_for_session/1" do
      session = insert(:scenario_session, %{nodes: 2})
      available_worker1 = insert(:worker, %{status: :online, session: nil})
      available_worker2 = insert(:worker, %{status: :online, session: nil})
      _unavailable_worker1 = insert(:worker, %{status: :busy})
      _unavailable_worker1 = insert(:worker, %{status: :online})

      {workers_num, workers} = Tests.acquire_workers_for_session(session)
      assert workers_num == 2
      assert [worker1, worker2] = workers

      assert worker1.id in [available_worker1.id, available_worker2.id]
      assert worker2.id in [available_worker1.id, available_worker2.id]

      assert worker1.status == :busy
      assert worker2.status == :busy
      assert worker1.session_id == session.id
      assert worker2.session_id == session.id

      session2 = insert(:scenario_session, %{nodes: 1})
      available_worker3 = insert(:worker, %{status: :online, session: nil})
      assert {1, [worker]} = Tests.acquire_workers_for_session(session2)

      assert worker.id == available_worker3.id
      assert worker.status == :busy
      assert worker.session_id == session2.id

      assert {0, []} = Tests.acquire_workers_for_session(session2)
    end

    test "get_worker!/1 returns the worker with given id" do
      worker_fixture = insert(:worker)
      worker = Tests.get_worker!(worker_fixture.id)
      assert Map.delete(worker_fixture, :session) == Map.delete(worker, :session)
    end

    test "get_worker_by_nodename/1" do
      ipv4 = "127.0.0.1"
      nodename = "dlt-test@#{ipv4}"

      refute Tests.get_worker_by_nodename(nodename)

      insert(:worker, %{ipv4: ipv4})

      assert Tests.get_worker_by_nodename(nodename)
    end

    test "create_worker/1 with valid data creates a worker" do
      assert {:ok, %Worker{} = worker} = Tests.create_worker(@valid_attrs)
      assert worker.ipv4 == "some ipv4"
      assert worker.password == "some password"
      assert worker.provider == :none
      assert worker.remote_id == "some remote_id"
      assert worker.status == :online
    end

    test "create_worker/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tests.create_worker(@invalid_attrs)
    end

    test "update_worker/2 with valid data updates the worker" do
      worker = insert(:worker)
      assert {:ok, %Worker{} = worker} = Tests.update_worker(worker, @update_attrs)
      assert worker.ipv4 == "some updated ipv4"
      assert worker.password == "some updated password"
      assert worker.provider == :hetzner
      assert worker.remote_id == "some updated remote_id"
      assert worker.status == :busy
    end

    test "update_worker/2 with invalid data returns error changeset" do
      worker_fixture = insert(:worker)
      assert {:error, %Ecto.Changeset{}} = Tests.update_worker(worker_fixture, @invalid_attrs)
      worker = Tests.get_worker!(worker_fixture.id)
      assert Map.delete(worker_fixture, :session) == Map.delete(worker, :session)
    end

    test "delete_worker/1 deletes the worker" do
      worker = insert(:worker)
      assert {:ok, %Worker{}} = Tests.delete_worker(worker)
      assert_raise Ecto.NoResultsError, fn -> Tests.get_worker!(worker.id) end
    end

    test "change_worker/1 returns a worker changeset" do
      worker = insert(:worker)
      assert %Ecto.Changeset{} = Tests.change_worker(worker)
    end
  end

  describe "scenario_session_results" do
    alias DLT.Tests.Scenario.Session.Result

    @valid_attrs %{
      code: "some code",
      second: 1,
      mean: 123,
      count: 42
    }
    @update_attrs %{
      code: "some updated code",
      second: 2,
      mean: 321,
      count: 123
    }
    @invalid_attrs %{code: nil, second: nil, mean: nil, count: nil}

    test "list_scenario_session_results/0 returns all scenario_session_results" do
      result_fixture = insert(:scenario_session_result)

      results =
        Tests.list_scenario_session_results() |> Enum.map(&Map.drop(&1, [:session, :worker]))

      expected = Map.drop(result_fixture, [:session, :worker])
      assert results == [expected]
    end

    test "get_result!/1 returns the result with given id" do
      result_fixture = insert(:scenario_session_result)
      result = Tests.get_result!(result_fixture.id)

      assert Map.drop(result_fixture, [:session, :worker]) ==
               Map.drop(result, [:session, :worker])
    end

    test "create_result/1 with valid data creates a result" do
      session = insert(:scenario_session)
      worker = insert(:worker, %{session: session})

      params =
        @valid_attrs
        |> Map.put(:session_id, session.id)
        |> Map.put(:worker_id, worker.id)

      assert {:ok, %Result{} = result} = Tests.create_result(params)
      assert result.code == "some code"
    end

    test "create_result/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tests.create_result(@invalid_attrs)
    end

    test "update_result/2 with valid data updates the result" do
      result = insert(:scenario_session_result)
      assert {:ok, %Result{} = result} = Tests.update_result(result, @update_attrs)
      assert result.code == "some updated code"
    end

    test "update_result/2 with invalid data returns error changeset" do
      result_fixture = insert(:scenario_session_result)
      assert {:error, %Ecto.Changeset{}} = Tests.update_result(result_fixture, @invalid_attrs)
      result = Tests.get_result!(result_fixture.id)

      assert Map.drop(result_fixture, [:session, :worker]) ==
               Map.drop(result, [:session, :worker])
    end

    test "delete_result/1 deletes the result" do
      result = insert(:scenario_session_result)
      assert {:ok, %Result{}} = Tests.delete_result(result)
      assert_raise Ecto.NoResultsError, fn -> Tests.get_result!(result.id) end
    end

    test "change_result/1 returns a result changeset" do
      result = insert(:scenario_session_result)
      assert %Ecto.Changeset{} = Tests.change_result(result)
    end
  end
end
