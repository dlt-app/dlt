defmodule DLT.Cloud.Provider.HetznerTest do
  use DLT.DataCase, async: true

  import ExUnit.CaptureLog

  @endpoint_servers "https://api.hetzner.cloud/v1/servers"

  describe "create_server/1" do
    setup do
      worker =
        insert(:worker, %{
          remote_id: nil,
          password: nil,
          provider: :hetzner,
          ipv4: nil,
          status: :initializing
        })

      ok_response = build(:hetzner_create_server_ok_response)

      {:ok, %{worker: worker, ok_response: ok_response}}
    end

    test "valid authorization header is sent", %{worker: worker, ok_response: ok_response} do
      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: @endpoint_servers} = env,
                                                           _opts ->
        assert {"Authorization", "Bearer test hetzner token"} in env.headers

        {:ok, %Tesla.Env{status: 201, body: ok_response}}
      end)

      DLT.Cloud.Provider.Hetzner.create_server(worker)
    end

    test "worker created with correct params", %{worker: worker, ok_response: ok_response} do
      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: 201, body: ok_response}}
      end)

      {:ok, %DLT.Tests.Worker{id: worker_id}} = DLT.Cloud.Provider.Hetzner.create_server(worker)

      worker = DLT.Tests.get_worker!(worker_id)
      assert worker.ipv4 == ok_response["server"]["public_net"]["ipv4"]["ip"]
      assert worker.password == ok_response["root_password"]
      assert worker.remote_id == to_string(ok_response["server"]["id"])
    end

    test "error", %{worker: worker} do
      err_code = 404
      err_response = %{"error" => "not_found"}

      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: err_code, body: err_response}}
      end)

      assert capture_log(fn ->
               assert DLT.Cloud.Provider.Hetzner.create_server(worker) ==
                        {:error, {err_code, err_response}}
             end) =~ "Hetzner responded with code #{err_code}:\n#{inspect(err_response)}"
    end
  end

  describe "list_servers/0" do
    test "returns list of servers" do
      servers = [:server1, :server2]

      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: 200, body: %{"servers" => servers}}}
      end)

      assert DLT.Cloud.Provider.Hetzner.list_servers() == {:ok, servers}
    end

    test "returns error" do
      status = 404
      error = %{"error" => "not found"}

      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: status, body: error}}
      end)

      assert DLT.Cloud.Provider.Hetzner.list_servers() == {:error, {status, error}}
    end
  end

  describe "delete_server/1" do
    setup do
      worker =
        insert(:worker, %{
          remote_id: nil,
          password: nil,
          provider: :hetzner,
          ipv4: nil,
          status: :initializing
        })

      delete_url = "#{@endpoint_servers}/#{worker.remote_id}"
      {:ok, %{worker: worker, delete_url: delete_url}}
    end

    test "calls to Hetzner to delete server", %{worker: worker, delete_url: delete_url} do
      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: ^delete_url}, _opts ->
        {:ok, %Tesla.Env{status: 200, body: %{"action" => %{"error" => nil}}}}
      end)

      {:ok, deleted_worker} = DLT.Cloud.Provider.Hetzner.delete_server(worker)

      assert deleted_worker.id == worker.id
    end

    test "Deletes worker from repo", %{worker: worker, delete_url: delete_url} do
      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: ^delete_url}, _opts ->
        {:ok, %Tesla.Env{status: 200, body: %{"action" => %{"error" => nil}}}}
      end)

      {:ok, _deleted_worker} = DLT.Cloud.Provider.Hetzner.delete_server(worker)

      refute DLT.Repo.get(DLT.Tests.Worker, worker.id)
    end

    test "error response from Hetzner", %{worker: worker, delete_url: delete_url} do
      error = %{"some" => "error"}

      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: ^delete_url}, _opts ->
        {:ok, %Tesla.Env{status: 200, body: %{"action" => %{"error" => error}}}}
      end)

      assert capture_log(fn ->
               assert DLT.Cloud.Provider.Hetzner.delete_server(worker) == {:error, error}
             end) =~ "Error deleting worker #{worker.id}: #{inspect(error)}"
    end
  end

  describe "list_locations/0" do
    test "returns list of locations" do
      url = "https://api.hetzner.cloud/v1/locations"

      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: ^url}, _opts ->
        {:ok, %Tesla.Env{status: 200, body: build(:hetzner_list_locations_ok_response)}}
      end)

      result = DLT.Cloud.Provider.Hetzner.list_locations()
      assert {:ok, [_, _] = locations} = result
      assert %{id: "1", country: "RU"} in locations
      assert %{id: "2", country: "UK"} in locations
    end

    test "returns error" do
      status = 404
      error = %{"error" => "not found"}

      url = "https://api.hetzner.cloud/v1/locations"

      Mox.expect(DLT.Cloud.Provider.HetznerMock, :call, fn %{url: ^url}, _opts ->
        {:ok, %Tesla.Env{status: status, body: error}}
      end)

      assert DLT.Cloud.Provider.Hetzner.list_locations() == {:error, {status, error}}
    end
  end
end
