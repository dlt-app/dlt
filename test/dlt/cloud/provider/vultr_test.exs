defmodule DLT.Cloud.Provider.VultrTest do
  use DLT.DataCase, async: true

  import ExUnit.CaptureLog

  @endpoint_servers "https://api.vultr.com/v2/instances"
  @endpoint_startup_scripts "https://api.vultr.com/v2/startup-scripts"

  describe "create_server/1" do
    setup do
      worker =
        insert(:worker, %{
          remote_id: nil,
          password: nil,
          provider: :vultr,
          ipv4: nil,
          status: :initializing
        })

      ok_response = build(:vultr_create_server_ok_response)

      {:ok, %{worker: worker, ok_response: ok_response}}
    end

    test "valid authorization header is sent", %{worker: worker, ok_response: ok_response} do
      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_startup_scripts},
                                                         _opts ->
        {:ok, %Tesla.Env{status: 202, body: build(:vultr_startup_scripts_ok_response)}}
      end)

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_servers} = env, _opts ->
        assert {"Authorization", "Bearer test vultr token"} in env.headers

        {:ok, %Tesla.Env{status: 202, body: ok_response}}
      end)

      DLT.Cloud.Provider.Vultr.create_server(worker)
    end

    test "startup script is created if not present", %{worker: worker, ok_response: ok_response} do
      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{
                                                           url: @endpoint_startup_scripts,
                                                           method: :get
                                                         },
                                                         _opts ->
        {:ok, %Tesla.Env{status: 202, body: build(:vultr_startup_scripts_empty_response)}}
      end)

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{
                                                           url: @endpoint_startup_scripts,
                                                           method: :post,
                                                           body: body
                                                         },
                                                         _opts ->
        decoded = Jason.decode!(body)
        assert match?(%{"name" => "dlt-worker-startup-script", "script" => _script}, decoded)
        {:ok, %Tesla.Env{status: 202, body: build(:vultr_create_startup_script_ok_response)}}
      end)

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: 202, body: ok_response}}
      end)

      DLT.Cloud.Provider.Vultr.create_server(worker)
    end

    test "worker created with correct params", %{worker: worker, ok_response: ok_response} do
      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_startup_scripts},
                                                         _opts ->
        {:ok, %Tesla.Env{status: 202, body: build(:vultr_startup_scripts_ok_response)}}
      end)

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: 202, body: ok_response}}
      end)

      {:ok, %DLT.Tests.Worker{id: worker_id}} = DLT.Cloud.Provider.Vultr.create_server(worker)

      worker = DLT.Tests.get_worker!(worker_id)
      # TODO
      # assert worker.ipv4 == ok_response["server"]["public_net"]["ipv4"]["ip"]
      assert worker.password == ok_response["instance"]["default_password"]
      assert worker.remote_id == to_string(ok_response["instance"]["id"])
    end

    test "error", %{worker: worker} do
      err_code = 404
      err_response = %{"error" => "not_found"}

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_startup_scripts},
                                                         _opts ->
        {:ok, %Tesla.Env{status: 202, body: build(:vultr_startup_scripts_ok_response)}}
      end)

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: err_code, body: err_response}}
      end)

      assert capture_log(fn ->
               assert DLT.Cloud.Provider.Vultr.create_server(worker) ==
                        {:error, {err_code, err_response}}
             end) =~ "Vultr responded with code #{err_code}:\n#{inspect(err_response)}"
    end
  end

  describe "list_servers/0" do
    test "returns list of servers" do
      servers = [:server1, :server2]

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: 200, body: %{"instances" => servers}}}
      end)

      assert DLT.Cloud.Provider.Vultr.list_servers() == {:ok, servers}
    end

    test "returns error" do
      status = 404
      error = %{"error" => "not found"}

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: @endpoint_servers}, _opts ->
        {:ok, %Tesla.Env{status: status, body: error}}
      end)

      assert DLT.Cloud.Provider.Vultr.list_servers() == {:error, {status, error}}
    end
  end

  describe "delete_server/1" do
    setup do
      worker =
        insert(:worker, %{
          remote_id: nil,
          password: nil,
          provider: :vultr,
          ipv4: nil,
          status: :initializing
        })

      delete_url = "#{@endpoint_servers}/#{worker.remote_id}"
      {:ok, %{worker: worker, delete_url: delete_url}}
    end

    test "calls to delete server", %{worker: worker, delete_url: delete_url} do
      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: ^delete_url}, _opts ->
        {:ok, %Tesla.Env{status: 204, body: nil}}
      end)

      {:ok, deleted_worker} = DLT.Cloud.Provider.Vultr.delete_server(worker)

      assert deleted_worker.id == worker.id
    end

    test "Deletes worker from repo", %{worker: worker, delete_url: delete_url} do
      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: ^delete_url}, _opts ->
        {:ok, %Tesla.Env{status: 204, body: nil}}
      end)

      {:ok, _deleted_worker} = DLT.Cloud.Provider.Vultr.delete_server(worker)

      refute DLT.Repo.get(DLT.Tests.Worker, worker.id)
    end

    test "error response", %{worker: worker, delete_url: delete_url} do
      error = %{"error" => "unable to delete", "status" => 500}

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: ^delete_url}, _opts ->
        {:ok, %Tesla.Env{status: 500, body: error}}
      end)

      assert capture_log(fn ->
               assert DLT.Cloud.Provider.Vultr.delete_server(worker) == {:error, error}
             end) =~ "Error deleting worker #{worker.id}: #{inspect(error)}"
    end
  end

  describe "list_locations/0" do
    test "returns list of locations" do
      url = "https://api.vultr.com/v2/regions"

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: ^url}, _opts ->
        {:ok, %Tesla.Env{status: 200, body: build(:vultr_list_locations_ok_response)}}
      end)

      result = DLT.Cloud.Provider.Vultr.list_locations()
      assert {:ok, [_, _] = locations} = result
      assert %{id: "1", country: "RU"} in locations
      assert %{id: "2", country: "UK"} in locations
    end

    test "returns error" do
      status = 404
      error = %{"error" => "not found"}

      url = "https://api.vultr.com/v2/regions"

      Mox.expect(DLT.Cloud.Provider.VultrMock, :call, fn %{url: ^url}, _opts ->
        {:ok, %Tesla.Env{status: status, body: error}}
      end)

      assert DLT.Cloud.Provider.Vultr.list_locations() == {:error, {status, error}}
    end
  end
end
