ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(DLT.Repo, :manual)

Mox.defmock(DLT.Cloud.Provider.HetznerMock, for: Tesla.Adapter)
Mox.defmock(DLT.Cloud.Provider.VultrMock, for: Tesla.Adapter)
