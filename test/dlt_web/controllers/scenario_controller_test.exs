defmodule DLTWeb.ScenarioControllerTest do
  use DLTWeb.ConnCase, async: true

  @create_attrs %{
    "duration" => 42,
    "name" => "some name",
    "nodes" => 42,
    "rps" => 42,
    "requests" => []
  }
  @update_attrs %{
    "duration" => 43,
    "name" => "some updated name",
    "nodes" => 43,
    "rps" => 43,
    "requests" => []
  }
  @invalid_attrs %{
    "duration" => nil,
    "name" => nil,
    "nodes" => nil,
    "rps" => nil,
    "requests" => []
  }

  describe "index" do
    test "lists all scenarios", %{conn: conn} do
      conn = get(conn, Routes.scenario_path(conn, :index))
      assert html_response(conn, 200) =~ "Scenarios"
    end
  end

  describe "new scenario" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.scenario_path(conn, :new))
      assert html_response(conn, 200) =~ "New Scenario"
    end
  end

  describe "create scenario" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.scenario_path(conn, :create), scenario: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.scenario_path(conn, :show, id)

      conn = get(conn, Routes.scenario_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Scenario ##{id}"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.scenario_path(conn, :create), scenario: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Scenario"
    end
  end

  describe "edit scenario" do
    test "renders form for editing chosen scenario", %{conn: conn} do
      scenario = insert(:scenario)
      conn = get(conn, Routes.scenario_path(conn, :edit, scenario))
      assert html_response(conn, 200) =~ "Edit Scenario"
    end
  end

  describe "update scenario" do
    test "redirects when data is valid", %{conn: conn} do
      scenario = insert(:scenario)
      conn = put(conn, Routes.scenario_path(conn, :update, scenario), scenario: @update_attrs)
      assert redirected_to(conn) == Routes.scenario_path(conn, :show, scenario)

      conn = get(conn, Routes.scenario_path(conn, :show, scenario))
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      scenario = insert(:scenario)
      conn = put(conn, Routes.scenario_path(conn, :update, scenario), scenario: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Scenario"
    end
  end

  describe "delete scenario" do
    test "deletes chosen scenario", %{conn: conn} do
      scenario = insert(:scenario)
      conn = delete(conn, Routes.scenario_path(conn, :delete, scenario))
      assert redirected_to(conn) == Routes.scenario_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.scenario_path(conn, :show, scenario))
      end
    end
  end
end
