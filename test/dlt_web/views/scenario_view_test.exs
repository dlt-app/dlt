defmodule DLTWeb.ScenarioViewTest do
  use DLTWeb.ConnCase, async: true

  test "session_results" do
    session = insert(:scenario_session)
    result1 = insert(:scenario_session_result, %{session: session, second: 1})
    result2 = insert(:scenario_session_result, %{session: session, second: 1})
    result3 = insert(:scenario_session_result, %{session: session, second: 2})
    result4 = insert(:scenario_session_result, %{session: session, second: 2})
    session = DLT.Repo.preload(session, :results)

    expected_count1 = result1.count + result2.count
    expected_count2 = result3.count + result4.count

    assert %{
             1 => %{count: ^expected_count1, mean: _mean1},
             2 => %{count: ^expected_count2, mean: _mean2}
           } = DLTWeb.ScenarioView.session_results(session)
  end
end
