defmodule DLT.Factory do
  use ExMachina.Ecto, repo: DLT.Repo

  alias DLT.Tests

  def scenario_factory(attrs) do
    requests = Map.get(attrs, :requests, fn -> [build(:scenario_request, %{scenario: nil})] end)

    scenario = %Tests.Scenario{
      duration: Enum.random(10..1000),
      name: sequence(:name, &"Test ##{&1}"),
      nodes: Enum.random(1..100),
      rps: Enum.random(10..1_000_000),
      requests: requests
    }

    scenario
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end

  def scenario_request_factory(attrs) do
    scenario = Map.get(attrs, :scenario, fn -> build(:scenario) end)

    request = %Tests.Scenario.Request{
      method: sequence(:method, [:get, :post]),
      url: sequence(:url, &"https://site#{&1}.tld"),
      headers: sequence(:headers, &~s("header1: value1#{&1}\nheader2: value2#{&1}")),
      body: sequence(:body, &~s({"param1": "value1 #{&1}", "param2": "value2 #{&1}"})),
      scenario: scenario
    }

    request
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end

  def scenario_session_factory(attrs) do
    status = Map.get(attrs, :status, Enum.random([:starting, :running, :finished]))
    duration = Enum.random(10..1000)

    {default_started_at, default_finished_at} =
      if status == :finished do
        seconds_ago = Enum.random(0..100)
        finished_at = NaiveDateTime.add(NaiveDateTime.utc_now(), -seconds_ago, :second)
        started_at = NaiveDateTime.add(finished_at, -duration, :second)
        {started_at, finished_at}
      else
        seconds_ago = Enum.random(0..100)
        started_at = NaiveDateTime.add(NaiveDateTime.utc_now(), -seconds_ago, :second)
        {started_at, nil}
      end

    started_at = Map.get(attrs, :started_at, default_started_at)
    finished_at = Map.get(attrs, :finished_at, default_finished_at)

    session = %Tests.Scenario.Session{
      nodes: Enum.random(1..100),
      duration: duration,
      rps: Enum.random(10..1_000_000),
      status: status,
      started_at: started_at,
      finished_at: finished_at,
      scenario: build(:scenario)
    }

    session
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end

  def worker_factory(attrs) do
    ipv4 = Map.get(attrs, :ipv4, build(:ipv4))

    worker = %Tests.Worker{
      provider: sequence(:provider, [:none, :hetzner, :vultr]),
      ipv4: ipv4,
      password: sequence(:password, &"password-#{&1}"),
      remote_id: to_string(System.unique_integer([:positive])),
      status: sequence(:status, [:initializing, :online, :busy]),
      session: build(:scenario_session)
    }

    worker
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end

  def scenario_session_result_factory do
    %Tests.Scenario.Session.Result{
      second: sequence(:result_second, & &1),
      code: sequence(:http_code, ["200", "201", "302", "404", "500"]),
      mean: :rand.uniform(10_000),
      count: :rand.uniform(1_000),
      session: build(:scenario_session),
      worker: build(:worker)
    }
  end

  def ipv4_factory(_attrs) do
    1..4
    |> Enum.map(fn _ -> Enum.random(1..255) end)
    |> Enum.join(".")
  end

  def hetzner_create_server_ok_response_factory do
    %{
      "server" => %{
        "id" => System.unique_integer([:positive]),
        "public_net" => %{"ipv4" => %{"ip" => build(:ipv4)}}
      },
      "root_password" => sequence(:hetzner_password, &"password-#{&1}")
    }
  end

  def vultr_create_server_ok_response_factory do
    %{
      "instance" => %{
        "id" => System.unique_integer([:positive]),
        "main_ip" => "0.0.0.0",
        "default_password" => sequence(:vultr_password, &"password-#{&1}")
      }
    }
  end

  def vultr_startup_scripts_ok_response_factory do
    %{
      "startup_scripts" => [
        %{
          "id" => System.unique_integer([:positive]),
          "name" => "dlt-worker-startup-script"
        }
      ]
    }
  end

  def vultr_startup_scripts_empty_response_factory do
    %{"startup_scripts" => []}
  end

  def vultr_create_startup_script_ok_response_factory do
    %{"startup_script" => %{"id" => System.unique_integer([:positive])}}
  end

  def hetzner_list_locations_ok_response_factory do
    %{
      "locations" => [
        %{"id" => 1, "name" => "one1", "country" => "RU"},
        %{"id" => 2, "name" => "two2", "country" => "UK"}
      ]
    }
  end

  def vultr_list_locations_ok_response_factory do
    %{
      "regions" => [
        %{"id" => "1", "city" => "Sochi", "country" => "RU"},
        %{"id" => "2", "city" => "London", "country" => "UK"}
      ]
    }
  end
end
